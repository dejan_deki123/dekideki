package ac.rs.uns.ftn.projekat;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;




//@ComponentScan(basePackages = {"ac.rs.uns.ftn"})
@SpringBootApplication
public class ProjekatApplication{

	public static void main(String[] args) {
		SpringApplication.run(ProjekatApplication.class, args);
	}

}
