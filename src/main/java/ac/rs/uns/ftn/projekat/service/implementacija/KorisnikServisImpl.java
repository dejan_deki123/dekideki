package ac.rs.uns.ftn.projekat.service.implementacija;

import ac.rs.uns.ftn.projekat.entity.Korisnik;
import ac.rs.uns.ftn.projekat.entity.Kupac;
import ac.rs.uns.ftn.projekat.repository.KorisnikRepo;
import ac.rs.uns.ftn.projekat.service.KorisnikServis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class KorisnikServisImpl implements KorisnikServis
{



    @Autowired
    private KorisnikRepo korisnikRepo;



    @Override
    public Korisnik create(Korisnik korisnik) throws Exception {
        if (korisnik.getID() != null) {
            throw new Exception("ID must be null!");
        }
        Korisnik noviKorisnik = this.korisnikRepo.save(korisnik);
        return noviKorisnik;
    }

    @Override
    public Boolean provera(String username, String lozinka) {
        if(this.korisnikRepo.findByUsername(username) != null) {
            if(this.korisnikRepo.findByUsername(username).getLozinka().equals(lozinka)) {
                        return true;
                }else {
                        return false;
                    }
                }else{
                        return false;
    }
}


    @Override
    public Korisnik findOne(String username) {
        Korisnik korisnik = new Korisnik();
        korisnik = this.korisnikRepo.findByUsername(username);
        return korisnik;
    }


    /*
    PROMENA ULOGE ZAPOSLENOG
   */
    @Override
    public Korisnik update(Korisnik korisnik) throws Exception {
        Korisnik korisnikUloga = this.korisnikRepo.getOne(korisnik.getID());
        if (korisnikUloga == null) {
            throw new Exception("Korisnik ne postoji!");
        }
        korisnikUloga.setUloga(korisnik.getUloga());

        Korisnik promenjen = this.korisnikRepo.save(korisnikUloga);
        return promenjen;
    }

    @Override
    public List<Korisnik> findAll() {
        List<Korisnik> korisnikList = this.korisnikRepo.findAll();
        return korisnikList;
    }


    @Override
    public Korisnik findByID(Long id) {
    Korisnik korisnik = this.korisnikRepo.getOne(id);
    return korisnik;
    }



/*
    @Override
    public Korisnik findByID(Long id){
        Korisnik korisnik = new Korisnik();
        korisnik = this.korisnikRepo.findByID(id);
        return korisnik;
    }

    @Override
    Korisnik findByID(Long id){
        Korisnik pronadjenKorisnik = new Korisnik();
            return pronadjenKorisnik;
    }
    */


}
