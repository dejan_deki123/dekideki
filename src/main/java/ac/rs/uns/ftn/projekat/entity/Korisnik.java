package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.io.Serializable;

enum Uloga{KUPAC, ADMINISTRATOR, DOSTAVLJAC}

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Korisnik implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="username")
    private String username;
    @Column(name="lozinka")
    private String lozinka;
    @Column(name="ime")
    private String ime;
    @Column(name="prezime")
    private String prezime;
    @Column(name="uloga")
    private Uloga uloga;
    @Column(name="telefon")
    private int telefon;
    @Column(name="email")
    private String email;
    @Column(name="adresa")
    private String adresa;

    @Override
    public String toString() {
        return "Kupac{" +
                "username='" + username + '\'' +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", lozinka='" + lozinka + '\'' +
                ", uloga='" + uloga + '\'' +
                ", telefon=" + telefon +
                ", mail='" + email + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public void setUloga(Uloga uloga) {
        this.uloga = uloga;
    }

    public void setTelefon(int telefon) {
        this.telefon = telefon;
    }

    public void setMail(String email) {
        this.email = email;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getUsername() {
        return username;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getLozinka() {
        return lozinka;
    }

    public Uloga getUloga() {
        return uloga;
    }

    public int getTelefon() {
        return telefon;
    }

    public String getEmail() {
        return email;
    }

    public String getAdresa() {
        return adresa;
    }



    public void setID(Long id) {
        this.id = id;
    }

    public Long getID() {
        return id;
    }




}