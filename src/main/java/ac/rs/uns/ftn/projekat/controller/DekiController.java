package ac.rs.uns.ftn.projekat.controller;
import ac.rs.uns.ftn.projekat.entity.Korisnik;
import ac.rs.uns.ftn.projekat.service.KorisnikServis;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class DekiController {

    @Autowired
    private KorisnikServis korisnikServis;


    @GetMapping("/")
    public String welcome() {
        return "home.html";
    }

    @GetMapping("/dodaj-korisnika")
    public String dodajKorisnika(Model model) {
        Korisnik korisnik = new Korisnik();
        model.addAttribute("korisnik", korisnik);
        return "dodaj-korisnika";
    }

    @PostMapping("/save-korisnik")
    public String saveKorisnik(@Valid @ModelAttribute Korisnik korisnik, BindingResult errors, Model model)
            throws Exception {
        // pozivanje metode servisa
        this.korisnikServis.create(korisnik);

        // redirekcija na tabelu koja prikazuje sve korisnike
        return "redirect:/korisnici";
    }

    //OVO JE ZA SVE KORISNKE
    @GetMapping("/korisnici")
    public String getKorisnici(Model model) {
        List<Korisnik> korisnikList = this.korisnikServis.findAll();
        model.addAttribute("korisnici", korisnikList);
        return "korisnici.html";
    }

    @GetMapping("/korisnici/{id}")
    public String getKorisnik(@PathVariable(name = "id") Long id, Model model) {
        Korisnik korisnik = this.korisnikServis.findByID(id);
        model.addAttribute("korisnik", korisnik);
        return "korisnik.html";
    }

    @GetMapping("/prijavi-korisnika")
    public String prijavaKor(Model model) {
        Korisnik korisnik = new Korisnik();
        model.addAttribute("korisnik", korisnik);
        return "prijavi-korisnika.html";
    }

    @PostMapping("/prijava-korisnika")
    public String prikaziKorisnika(@Valid @ModelAttribute Korisnik korisnik, BindingResult errors, Model model){

        Korisnik ulogovanKorisnik = new Korisnik();
        ulogovanKorisnik = korisnikServis.findOne(korisnik.getUsername());

        if(korisnikServis.provera(korisnik.getUsername(), korisnik.getLozinka())){
            //model.addAttribute("korisnik", ulogovanKorisnik.getKorisnickoime());
            return "/prijava-korisnika.html";
        }
        else{
            //model.addAttribute("korisnik","greska");
            return "/prijavagreske.html";
        }
    }
/*
    @PostMapping("/save-korisnik")
    public String saveKorisnik(@Valid @ModelAttribute Korisnik korisnik, BindingResult errors, Model model)
            throws Exception {
        // pozivanje metode servisa
        this.korisnikServis.create(korisnik);

        // redirekcija na tabelu koja prikazuje sve korisnike
        return "redirect:/korisnici";
    }
*/
}
