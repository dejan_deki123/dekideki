package ac.rs.uns.ftn.projekat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ac.rs.uns.ftn.projekat.entity.Artikal;

import java.util.List;

public interface ArtikalRep extends JpaRepository<Artikal,Long> {
    List<ArtikalRep> findByCena(int cena);
    List<ArtikalRep> findAllById(Long id);
    List<ArtikalRep> findByKategorija(String kategorija);
    List<ArtikalRep> findByNaziv(String naziv);
    List<ArtikalRep> findByOpisArtikla(String opisArtikla);
    List<ArtikalRep> findByKolicina(int kolicina);
}
