package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Artikal implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="naziv")
    private String naziv;
    @Column(name="opisartikla")
    private String opisArtikla;
    @Column(name="cena")
    private double cena;
    @Column(name="kolicina")
    private int kolicina;
    @Column(name="kategorija")
    private String kategorija;

    @ManyToOne
    private Kupac kupac;

    @ManyToOne
    private Korpa korpa;

    public String getNaziv() {
        return naziv;
    }

    public String getOpisArtikla() {
        return opisArtikla;
    }

    public double getCena() {
        return cena;
    }

    public int getKolicina() {
        return kolicina;
    }

    public String getKategorija() {
        return kategorija;
    }

    public Kupac getKupac() {
        return kupac;
    }

    public Long getId() {
        return id;
    }

    public Korpa getKorpa() {
        return korpa;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setOpisArtikla(String opisArtikla) {
        this.opisArtikla = opisArtikla;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public void setKupac(Kupac kupac) {
        this.kupac = kupac;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setKorpa(Korpa korpa) {
        this.korpa = korpa;
    }

    @Override
    public String toString() {
        return "ArtikalRepository{" +
                "naziv='" + naziv + '\'' +
                ", opisArtikla='" + opisArtikla + '\'' +
                ", cena=" + cena +
                ", kolicina=" + kolicina +
                ", kategorija='" + kategorija + '\'' +
                ", kupac=" + kupac +
                '}';
    }
}
