package ac.rs.uns.ftn.projekat.repository;

import ac.rs.uns.ftn.projekat.entity.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KorisnikRepo extends JpaRepository<Korisnik, Long> {

    List<Korisnik> findAllByUlogaOrderByIme(String uloga);


    List<Korisnik> findByImeOrPrezime(String ime, String prezime);


    List<Korisnik> findByImeIgnoreCase(String ime);

    Korisnik findByUsername(String username);


   // Korisnik findOne(String korisnickoime);

    List<Korisnik> findByUloga(String uloga);
}
