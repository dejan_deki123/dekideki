package ac.rs.uns.ftn.projekat.repository;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KorpaRep extends JpaRepository<Artikal,Long> {
}
