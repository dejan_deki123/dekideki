package ac.rs.uns.ftn.projekat.service;

import ac.rs.uns.ftn.projekat.entity.Korisnik;


import java.util.List;



public interface KorisnikServis {


    Korisnik create(Korisnik korisnik) throws Exception;

    Boolean provera(String username, String lozinka);

    Korisnik findOne(String username);


    Korisnik findByID(Long id);

    Korisnik update(Korisnik korisnik) throws Exception;

    List<Korisnik> findAll();

//    Korisnik findByID(Long id);


}
